package controller;

import java.util.List;
import java.util.PriorityQueue;
import model.bst.BinarySearchTree;

public class Controller {

  private PriorityQueue<Integer> priorityQueue;
  private BinarySearchTree<Integer> binarySearchTree;

  public Controller() {
    priorityQueue = new PriorityQueue<>();
    binarySearchTree = new BinarySearchTree<>();
  }

  public void initQueue(List values) {
    for (Object o : values) {
      priorityQueue.add((Integer) o);
    }
  }

  public String queuePollTest() {
    priorityQueue.poll();
    priorityQueue.poll();
    priorityQueue.poll();
    return priorityQueue.toString();
  }

  public void initTree(List values) {
    for (Object o : values) {
      binarySearchTree.add((Integer) o);
    }
  }

  public String treeRemoveTest(Integer... removeValues) {
    for (Integer i : removeValues) {
      binarySearchTree.remove(i);
    }
    return binarySearchTree.getValues().toString();
  }
}
