package model.priority_queue;

import java.util.ArrayList;

public class PriorityQueue<E extends Comparable> {
  private ArrayList<E> list;

  public PriorityQueue() {
    this.list = new ArrayList<E>();
  }

  public PriorityQueue(ArrayList<E> items) {
    this.list = items;
    buildQueue();
  }

  public void add(E item) {
    list.add(item);
    int i = list.size() - 1;
    int parentIndex = getListMiddleIndex(i);
    while ((parentIndex != i) && (list.get(i).compareTo(list.get(parentIndex)) < 0)) {
      swap(i, parentIndex);
      i = parentIndex;
      parentIndex = getListMiddleIndex(i);
    }
    buildQueue();
  }

  private void buildQueue() {
    for (int i = list.size() / 2; i >= 0; i--) {
      prioritize(i);
    }
  }

  private void prioritize(int i) {
    int left = left(i);
    int right = right(i);
    int smallest = -1;
    // find the smallest key between current node and its children.
    if ((left <= list.size() - 1) && (list.get(left).compareTo(list.get(i)) < 0)) {
      smallest = left;
    } else {
      smallest = i;
    }
    if ((right <= list.size() - 1) && (list.get(right).compareTo(list.get(smallest)) < 0)) {
      smallest = right;
    }
    // if the smallest key is not the current key then bubble-down it.
    if (smallest != i) {
      swap(i, smallest);
      prioritize(smallest);
    }
  }

  public E pull() {
    if (list.size() == 0) {
      throw new IllegalStateException("Queue is EMPTY");
    } else if (list.size() == 1) {
      E min = list.remove(0);
      return min;
    }
    // remove the last item ,and set it as new root
    E min = list.get(0);
    E lastItem = list.remove(list.size() - 1);
    list.set(0, lastItem);
    // bubble-down until heap property is maintained
    prioritize(0);
    // return min key
    return min;
  }

  public E getMin() {
    return list.get(0);
  }

  public boolean isEmpty() {
    return list.size() == 0;
  }

  private int right(int i) {
    return 2 * i + 2;
  }

  private int left(int i) {
    return 2 * i + 1;
  }

  private int getListMiddleIndex(int i) {
    return (i % 2 == 1) ? (i / 2) : ((i - 1) / 2);
  }

  private void swap(int i, int parentIndex) {
    E temp = list.get(parentIndex);
    list.set(parentIndex, list.get(i));
    list.set(i, temp);
  }

  @Override
  public String toString() {
    return "PriorityQueue{" +
        "list=" + list +
        '}';
  }
}
