package view;

import controller.Controller;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class View {

  private Controller controller;
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Scanner input = new Scanner(System.in);
  private static final Logger logger = LogManager.getLogger(View.class);
  private List testValues;

  public View() {
    controller = new Controller();
    menu = new LinkedHashMap<>();
    menu.put("1", "  1 - Run Priority Queue test");
    menu.put("2", "  2 - Run Binary Search Tree test");
    menu.put("q", "  q - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::pressButton1);
    methodsMenu.put("2", this::pressButton2);

    testValues = new ArrayList<Integer>();
    testValues.add(20);
    testValues.add(10);
    testValues.add(30);
    testValues.add(15);
    testValues.add(1);
    testValues.add(67);
    testValues.add(15);
  }

  private void pressButton1() {
    controller.initQueue(testValues);
    logger.info("Priority queue has created with test values " + testValues );
    logger.info("Priority queue after poll test " + controller.queuePollTest());
  }

  private void pressButton2() {
    controller.initTree(testValues);
    logger.info("Binary search tree has created with test values " + testValues );
    logger.info("Priority queue after poll test " + controller.treeRemoveTest());
  }

  private void outputMenu() {
    System.out.println("\nMENU:");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void show() {
    String keyMenu;
    do {
      outputMenu();
      System.out.print("Please, select menu option: ");
      keyMenu = input.nextLine();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("q"));
  }

}
