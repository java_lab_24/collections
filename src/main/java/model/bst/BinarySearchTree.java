package model.bst;

import java.util.LinkedList;
import java.util.List;

public class BinarySearchTree<E extends Comparable> {

  private Node root;

  public BinarySearchTree() {
    root = null;
  }

  public void add(E value) {
    root = add(root, value);
  }

  private Node add(Node current, E value) {
    if (current == null) {
      return new Node(value);
    }
    if (value.compareTo(current.value) < 0) {
      current.leftChild = add(current.leftChild, value);
    } else if (value.compareTo(current.value) > 0) {
      current.rightChild = add(current.rightChild, value);
    } else {
      current.repetitionsNumber++;
    }
    return current;
  }

  public boolean remove(E value) {
    Node nodeToRemove = find(root, value);
    if (nodeToRemove != null) {
      if (nodeToRemove.repetitionsNumber != 0) {
        nodeToRemove.repetitionsNumber--;
      } else {
        if (nodeToRemove.isChildless()) {
          removeChildless(nodeToRemove);
        }
        if (nodeToRemove.isWithOneChildren()) {
          removeWithOneChildren(nodeToRemove);
        }
        if (nodeToRemove.isWithTwoChildren()) {
          removeWithTwoChildren(nodeToRemove);
        }
      }
      return true;
    }
    return false;
  }

  private void removeChildless(Node deleteNode) {
    Node parent = getParent(deleteNode);
    if (parent.leftChild != null) {
      if (parent.leftChild.equals(deleteNode)) {
        parent.leftChild = null;
      } else {
        parent.rightChild = null;
      }
    } else {
      parent.rightChild = null;
    }
  }

  private void removeWithOneChildren(Node deleteNode) {
    Node parent = getParent(deleteNode);
    Node child = (deleteNode.leftChild != null) ? deleteNode.leftChild : deleteNode.rightChild;
    if (parent.leftChild != null) {
      if (parent.leftChild.equals(deleteNode)) {
        parent.leftChild = child;
      } else {
        parent.rightChild = child;
      }
    } else {
      parent.rightChild = child;
    }
  }

  private void removeWithTwoChildren(Node deleteNode) {
    if (deleteNode.equals(root)) {
      removeRoot();
      return;
    }
    Node parent = getParent(deleteNode);
    if (deleteNode.rightChild.leftChild == null) {
      deleteNode.rightChild.leftChild = deleteNode.leftChild;
      if (parent.leftChild != null) {
        if (parent.leftChild.equals(deleteNode)) {
          parent.leftChild = deleteNode.rightChild;
        } else {
          parent.rightChild = deleteNode.rightChild;
        }
      } else {
        parent.rightChild = deleteNode.rightChild;
      }
      return;
    }
    Node successorNode = getSuccessor(deleteNode);
    successorNode.rightChild = deleteNode.rightChild;
    successorNode.leftChild = deleteNode.leftChild;
    getParent(successorNode).leftChild = null;
    if (parent.leftChild != null) {
      if (parent.leftChild.equals(deleteNode)) {
        parent.leftChild = successorNode;
      } else {
        parent.rightChild = successorNode;
      }
    } else {
      parent.rightChild = successorNode;
    }
  }

  public E getRoot() {
    return (E)root.value;
  }

  private void removeRoot(){
    if (root.rightChild.leftChild == null) {
      root.rightChild.leftChild = root.leftChild;
      root = root.rightChild;
    } else {
      Node successorNode = getSuccessor(root);
      successorNode.leftChild = root.leftChild;
      successorNode.rightChild = root.rightChild;
      root = successorNode;
      getParent(successorNode).leftChild = null;
    }
  }

  private Node getSuccessor(Node node){
    return getMin(node.rightChild);
  }

  public boolean contains(E value) {
    return find(root, value) != null;
  }

  private Node find(Node current, E searchValue) {
    if (current != null) {
      if (searchValue.compareTo(current.value) == 0) {
        return current;
      }
      if (searchValue.compareTo(current.value) < 0) {
        current = find(current.leftChild, searchValue);
      } else if (searchValue.compareTo(current.value) > 0) {
        current =  find(current.rightChild, searchValue);
      }
    }
    return current;
  }

  public E getMin() {
    Node smallest = getMin(root);
    return (E) smallest.value;
  }

  private Node getMin(Node current) {
    if (current == null) {
      throw new NullPointerException("Node is empty");
    }
    if (current.leftChild == null) {
      return current;
    } else {
      current = getMin(current.leftChild);
    }
    return current;
  }

  private Node getParent(Node child) {
    Node current = root;
    while ((current.leftChild != child) && (current.rightChild != child)) {
      if (current.value.compareTo(child.value) > 0) {
        current = current.leftChild;
      } else{
        current = current.rightChild;
      }
    }
    return current;
  }

  public List getValues() {
    LinkedList<E> values = new LinkedList<>();
    valuesToList(root, values);
    return values;
  }

  private void valuesToList(Node node, List<E> list) {
    if(node != null) {
      valuesToList(node.leftChild, list);
      list.add((E) node.value);
      valuesToList(node.rightChild, list);
    }
  }

  private class Node<E extends Comparable> {

    private E value;
    private int repetitionsNumber;
    private Node leftChild;
    private Node rightChild;

    Node(E value){
      this.value = value;
      repetitionsNumber = 0;
      leftChild = null;
      rightChild = null;
    }

    public boolean equals(Node node) {
      return this.value.equals(node.value);
    }

    private boolean isChildless(){
      return (this.leftChild == null) && (this.rightChild == null);
    }

    private boolean isWithOneChildren(){
      if (this.rightChild == null && this.leftChild != null) {
        return true;
      } else if (this.leftChild == null && this.rightChild != null) {
        return true;
      }
      return false;
    }

    private boolean isWithTwoChildren(){
      return (this.leftChild != null) && (this.rightChild != null);
    }
  }

}
